const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

app.use(bodyParser.urlencoded());
app.use(bodyParser.json());
app.use(cors());

const productos = [
  {
    id: 1,
    nombre: 'MBP 15',
    descripcion: 'modelo del 2016 15 pulgadas',
    cantidad: 50,
    precio: 2700
  },
  {
    id: 2,
    nombre: 'MBP 13',
    descripcion: 'modelo del 2016 13 pulgadas',
    cantidad: 25,
    precio: 1850
  },
  {
    id: 3,
    nombre: 'Ipad 2018',
    descripcion: 'modelo con pencil',
    cantidad: 100,
    precio: 380
  },
  {
    id: 4,
    nombre: 'Iphone X',
    descripcion: 'modelo aniversario',
    cantidad: 5,
    precio: 1100
  }
];

app.get('/productos', (req, res) => {
  const data = productos;
    res.header('Access-Contrrol-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With');
    res.json(data);
});

app.get('/producto/:id', (req, res) => {
  const id = req.params.id;
  const data = productos.find(p => p.id == id);
  res.header('Access-Contrrol-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With');
  res.json(data);
});

app.post('/producto', (req, res) => {
  console.log(req.body);
  if (req.body.producto && req.body.producto.id) {
    const found = productos.find(p => p.id == req.body.producto.id)
    const index = productos.indexOf(found);
    productos[index] = req.body.producto;
  } else if (req.body.producto) {
    productos.push(req.body.producto);
  }
  res.header('Access-Contrrol-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With');
  res.json({
    success: true,
    message: 'guardado correctamente'
  });
});

app.get('/deleteProducto/:id', (req, res) => {
  const id = req.params.id;
  const found = productos.find(p => p.id == id);
  const index = productos.indexOf(found);
  if (index > -1) {
    productos.splice(index, 1);
  }
  res.header('Access-Contrrol-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With');
  res.json({
    success: true,
    message: 'borrado correctamente'
  });
});

app.listen(3000, function() {
    console.log('served at port 3000');
})
